#!/bin/bash
set -e
. tests/lib

t-dependencies DEBORIG git-debpush

t-debpolicy

t-archive-none example
t-git-none
t-worktree 1.0

v=1.0-1

cd $p
git checkout -b native 

git checkout --orphan upstream quilt-tip-2
git rm -rf debian
git commit -m 'pseudo-upstream'
upstreamtag=UPSTREAM/RELEASE/1.0
git tag $upstreamtag

git checkout -B master quilt-tip-2

echo foo >bar
git add bar
git commit -m"corrupt the upstream source to test upstream-nonidentical check"

t-tagupl-settings

tagname=test-dummy/$v

t-expect-fail "the upstream source in tag $upstreamtag is not identical to the upstream source in refs/heads/master" \
t-tagupl-test --quilt=gbp --upstream=$upstreamtag

git reset --hard HEAD~1

t-expect-fail "upstream tag $upstreamtag is not an ancestor of refs/heads/master" \
t-tagupl-test --quilt=gbp --upstream=$upstreamtag

t-expect-fail "upstream tag $upstreamtag is not an ancestor of refs/heads/master" \
t-tagupl-test --quilt=gbp --force=suite --force=no-such-force-option --upstream=$upstreamtag

t-tagupl-test --quilt=gbp --force=suite --force=no-such-force-option-1 \
              --force=upstream-nonancestor,no-such-force-option-2 \
              --upstream=$upstreamtag
t-pushed-good master

# todo: test each miss/rejection

ident=ok

git cat-file tag $tagname >../basetag
v=1.0-2
tagname=test-dummy/$v

perl -i -ne '
	next if $.==1../^$/;
	next if m/^----/..0;
	s/\b1\.0-1\b/1.0-2/g;
	print or die $!;
' ../basetag

mv-ident () {
	local f=$tmp/$1
	if test -e $f; then
		mv $f $f--$ident
	fi
}

next-mangle () {
	mv-ident tagupl/overall.log
	mv-ident sendmail.log
	ident=$1
}

with-mangled () {
	local perl=$1
	perl <../basetag >../badtag-$ident -pe "$perl"
	git tag -u Senatus -f -s -m "$(cat ../badtag-$ident)" "$tagname"

	LC_MESSAGES=C \
	t-tagupl-run-drs $tmp/$p
}

expect-quit () {
	next-mangle "$1"
	local perl=$2
	local mregexp=$3
	with-mangled "$perl"
	egrep ": $mregexp" ../tagupl/overall.log
}

expect-email () {
	next-mangle "$1"
	local perl=$2
	local mregexp=$3
	with-mangled "$perl"
	egrep 'Was not successful' ../sendmail.log
	egrep "$mregexp" ../sendmail.log
	egrep ": failed, emailed" ../tagupl/overall.log
}

raw-mangled () {
	git update-ref refs/tags/$tagname \
		$(git hash-object -w -t tag ../tagobj-$ident)
	t-tagupl-run-drs $tmp/$p
}

tagname=test-dummy/1.2
t-expect-fail E:'failed command: git fetch' \
t-tagupl-run-drs $tmp/$p

tagname=splorf/$v     ; expect-quit baddistro '' 'tag name not for us'
tagname=test-dummy/1,2; expect-quit badver    '' 'tag name not for us'
tagname=test-dummy/$v

expect-quit noplease s/please-upload/plunk/ 'tag missing please-upload'

expect-email vermatch 's/^example release /$&3/' 'reject: version mismatch'

expect-email unkinfo 's/^\[dgit please-upload/$& Rejectme/' \
	'unknown dgit info in tag'

expect-quit unkdistro 's/test-dummy/ubuntu/ if m/^\[dgit/' \
	'not for this distro'

expect-email notsplit 's/ split / no-split /' 'reject: missing "split"'

expect-email upsnot1 's/ upstream=/ uxstream=/' \
	'reject: upstream tag and not commitish'

expect-email upsnot2 's/ upstream-tag=/ uxstream-tag=/' \
	'reject: upstream tag and not commitish'

expect-email bupstag1 's/ upstream-tag=/$&:/' \
	"failed command: git check-ref-format"

expect-email bupstag2 's/ upstream-tag=/$&x/' \
	"[Cc]ouldn't find remote ref refs/tags/xUPSTREAM"

expect-email wrongver '' 'mismatch: changelog Version'

v=1.0-2

t-dch-commit -v $v -m bump

expect-email upsmism 's/ upstream=/$&3/' \
	"but tag refers to"

expect-email wrongpkg 's/^example /explosive /' 'mismatch: changelog Source'

# we are going to trash $p because it will contain corrupted objects
# which makes our end-of-test fsck fail
cp -al ../$p ../$p.save
cd ../$p

git cat-file tag $tagname >../raw-base

next-mangle sigfail
perl -pe <../raw-base >../tagobj-$ident 's/ split / split ignoreme /'
raw-mangled
grep 'gpgv: BAD signature' ../sendmail.log

next-mangle nosig
perl -ne <../raw-base >../tagobj-$ident 'print unless m/^-----/..0'
raw-mangled
grep 'missing signature' ../sendmail.log

git cat-file tag test-dummy/1.0-1 >../raw-base

next-mangle badtagger
perl -pe <../raw-base '
	s/\+\d+$/xyz/ if m/^tagger /;
	exit 0 if m/^$/;
' >../tagobj-$ident
echo >>../tagobj-$ident
cat ../basetag >>../tagobj-$ident
raw-mangled
grep 'failed to fish tagger out of tag' ../tagupl/overall.log

cd ..
rm -rf $p
mv $p.save $p

t-ok
