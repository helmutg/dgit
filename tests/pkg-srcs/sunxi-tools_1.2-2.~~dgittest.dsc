Format: 3.0 (quilt)
Source: sunxi-tools
Binary: sunxi-tools
Architecture: any
Version: 1.2-2.~~dgittest
Maintainer: Ian Campbell <ijc@hellion.org.uk>
Homepage: http://linux-sunxi.org/Sunxi-tools
Standards-Version: 3.9.5
Vcs-Browser: http://git.debian.org/?p=collab-maint/sunxi-tools.git
Vcs-Git: git://git.debian.org/collab-maint/sunxi-tools.git
Build-Depends: debhelper (>= 9), pkg-config, libusb-1.0-0-dev, u-boot-tools
Package-List: 
 sunxi-tools deb utils optional
Checksums-Sha1: 
 2457216dbbf5552c413753f7211f7be3db6aff54 35076 sunxi-tools_1.2.orig.tar.gz
 6f30698cd897b350a4f92b2b5dded69adca6f82e 5182 sunxi-tools_1.2-2.~~dgittest.debian.tar.gz
Checksums-Sha256: 
 03a63203ff79389e728d88ad705e546aa6362a6d08b9901392acb8639998ef95 35076 sunxi-tools_1.2.orig.tar.gz
 0a513f3254d245b59aaffbeb5c43159a6461617c1f6f3c6824646c4259cda406 5182 sunxi-tools_1.2-2.~~dgittest.debian.tar.gz
Files: 
 dbc55f60559f9db497559176c3c753dd 35076 sunxi-tools_1.2.orig.tar.gz
 a6ec0eb0d897b0121dc978fc00db2ea6 5182 sunxi-tools_1.2-2.~~dgittest.debian.tar.gz
